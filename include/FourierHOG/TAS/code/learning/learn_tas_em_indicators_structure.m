function model = learn_tas_em_indicators_structure(train, tas_params)
% function model = learn_tas_em_indicators_structure(train, tas_params)
%
%  Learns the parameters of a TAS model using incomplete data, also
%  determining which relationships to use.
%
    rand('state', 1);
    % Compute Relationship Variables
    train = update_relationship_indicators(train);

    % Set the l variable values
    train.l = train.truth_labels;
    F = cell2mat(train.f');
    
    % Cardinalities
    model.QK = tas_params.QK;
    model.LK = 2;
    model.RK = size(train.r{1},3);
    model.FN = size(train.f{1},2);
    fprintf('Learning TAS model with:\n');
    fprintf('QK = %d, LK = %d, RK = %d, FN = %d\n', model.QK, model.LK, ...
            model.RK, model.FN);
    model.region_f_model       = 'gaussian';
    model.active_rs            = zeros(1,model.RK);

    iter = 0;
    while(1)
        iter = iter + 1;

        % Perform EM parameter learning with current active R's
        if(iter==1)
            [model, exp_ss] = learn_tas_em_indicators(train, tas_params, ...
                                                      model.active_rs, 1e-4);
        else
            [model, exp_ss] = learn_tas_em_indicators(train, tas_params, ...
                                                      model.active_rs, ...
                                                      1e-6, exp_ss);
        end

        if(iter==2), break; end;
        
        % Choose Best Structure given these SuffStats
        old_active_rs = model.active_rs;
        model.active_rs = find_best_structure_greedy(train, model, ...
                                                     exp_ss, tas_params, ...
                                                     old_active_rs);
        
        
        % If Structure is the same
        if(all(old_active_rs==model.active_rs))
            fprintf('Converged with Active Rs: ');
            fprintf('%g ', model.active_rs);
            fprintf('\n');
            break;
        end
    end

    return
end

