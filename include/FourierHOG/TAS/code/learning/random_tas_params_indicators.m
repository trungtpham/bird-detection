function model = random_tas_params_indicators(model, F)
% function model = random_tas_params(model, F)
% 
%   Assign random parameters
%  model: TAS model format
%     - model.w_l              = Logistic Regression Parameters for P(l | b)
%     - model.theta_q(i)       = Multinomial parameters for P(q = i)
%     - model.theta_r(k,i,l,q) = Multinomial parameters for P(r_k=i|l,q)

% Theta_Q
model.theta_q = rand_cpd(model.QK);

% Initial Clustering for centroids
if(strcmp('gaussian', model.region_f_model))
    fprintf('Clustering into %d initial centroids\n', model.QK);
    [IDX, CENTROIDS] = kmeans(F, model.QK);
end

for q = 1:model.QK
    if(strcmp('gaussian', model.region_f_model))
        % MU,COV
        model.mu_f(:,q)    = CENTROIDS(q,:);
        model.cov_f(:,:,q) = eye(model.FN);
        model.prec_f(:,:,q) = eye(model.FN);
        model.logdet_prec_f(q) = logdet(model.prec_f(:,:,q));
    else
        error(sprintf('UNKNOWN REGION FEATURE MODEL: %s', model.region_f_model));
    end    
        
    % Theta_R
    for l = 1:model.LK
        for k = 1:model.RK
            model.theta_r(k,:,l,q) = [0.99 0.01];
        end
    end    
end

return
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cpd = rand_cpd(K)
  cpd = rand(K,1) + 1;
  cpd = (1.0/sum(cpd)) * cpd;
  return
end
