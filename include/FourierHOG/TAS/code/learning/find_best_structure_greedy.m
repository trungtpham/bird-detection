%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  active_rs = find_best_structure_greedy(train, model, exp_ss, ...
                                                 tas_params, init_active_rs)

  fprintf('****************************\n');
  fprintf('Structure Search\n');
  active_rs = init_active_rs;

  % Compute Score_0 (score of input)
  model_0 = model;
  model_0 = estimate_tas_parameters_indicators(train, exp_ss, model_0, ...
                                               tas_params);
  score_0 = structure_score(train, model_0, tas_params);
  fprintf('0: SCORE = %g\n', score_0);

  update = 1;
  while(update)
      update = 0;

      % For each possible relationship
      best_delta = 0; best_k = 0;
      for k = 1:model.RK
          
          % Skip
          if(isfield(tas_params,'skip_rs'))
              if(any(tas_params.skip_rs==k)), continue; end;
          end
          
          % Learn the parameters with the given SuffStats
          model_k = model_0;
          model_k.active_rs(k) = 1-model_k.active_rs(k);
          model_k = estimate_tas_parameters_indicators(train, exp_ss, model_k, ...
                                                       tas_params);
      
          % Compute the Score
          score_k = structure_score(train, model_k, tas_params);
          delta_k = score_k - score_0;
      
          % If Score_k - Score_0 > 0, accept the relationship
          fprintf('%02d (%g->%g): DELTA = %g\n', k, model_0.active_rs(k), ...
                  model_k.active_rs(k), delta_k);
          if(delta_k > best_delta)
              best_delta = delta_k;
              best_k     = k;
          end
      end

      % Update if you found a good one
      if(best_k > 0)
          fprintf('Choosing to toggle %g\n', best_k);
          update = 1;
          model_0.active_rs(best_k) = 1-model_0.active_rs(best_k);
          model_0 = estimate_tas_parameters_indicators(train, exp_ss, ...
                                                       model_0, tas_params);
          score_0 = structure_score(train, model_0, tas_params);
          fprintf('New Active Rs: ');
          fprintf(' %g', model_0.active_rs);
          fprintf('\n');
      end
      
  end

  fprintf('Finishing with SCORE = %g\n', score_0);
  active_rs = model_0.active_rs;
  
  fprintf('\n\n');
  fprintf('Active Rs: ');
  fprintf('%g ', active_rs);
  fprintf('\n');
  fprintf('****************************\n');
  fprintf('\n\n');
  
  return

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  active_rs = find_best_structure_oneshot(train, model, exp_ss, tas_params)

  error('Must fix before using!');

  fprintf('****************************\n');
  fprintf('Structure Search\n');
  active_rs = model.active_rs;

  % Compute Score_0 (score of all inactive)
  model_0 = model;
  model_0.active_rs = zeros(1,model.RK);
  model_0 = estimate_tas_parameters_indicators(train, exp_ss, model_0, ...
                                               tas_params);
  LL_0    = tas_data_ll_indicators(train, model_0);
  bic_0   = bic_penalty(train, model_0);
  score_0 = LL_0 - bic_0;
  fprintf('0: LL = %g, BIC = %g, SCORE = %g\n', LL_0, bic_0, score_0);

  % For each possible relationship
  delta_sum = 0;
  for k = 1:model.RK
      
      % Learn the parameters with the given SuffStats
      model_k = model_0;
      model_k.active_rs(k) = 1;
      model_k = estimate_tas_parameters_indicators(train, exp_ss, model_k, ...
                                                   tas_params);
      
      % Compute the Likelihood of the Data
      LL_k    = tas_data_ll_indicators(train, model_k);

      % Add the BIC Penalty
      bic_k   = bic_penalty(train, model_k);
      score_k = LL_k - bic_k;
      delta_k = score_k - score_0;
      
      % If Score_k - Score_0 > 0, accept the relationship
      fprintf('%g: LL = %g, BIC = %g, DELTA_SCORE = %g\n', k, LL_k, ...
              bic_k, delta_k);
      if(delta_k > 0)
          active_rs(k) = 1;
          delta_sum = delta_sum + delta_k;
      end
  end

  % Sanity Check
  model_full = model;
  model_full.active_rs = active_rs;
  model_full = estimate_tas_parameters_indicators(train, exp_ss, ...
                                                  model_full, tas_params);
  
  % Compute the Lifullelihood of the Data
  LL_full    = tas_data_ll_indicators(train, model_full);
  
  % Add the BIC Penalty
  bic_full   = bic_penalty(train, model_full);
  score_full = LL_full - bic_full;
  delta_full = score_full - score_0;
  fprintf('FULL DELTA = %g\tSUM DELTA = %g\n', delta_full, delta_sum);

  fprintf('\n\n');
  fprintf('Active Rs: ');
  fprintf('%g ', active_rs);
  fprintf('\n');
  fprintf('****************************\n');
  fprintf('\n\n');
  
  return

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function score = structure_score(train, model, tas_params)

  if(1) % DISCRIMINATIVE P(T|R)
      infer_params.INIT    = 'rand';
      if(length(train.name) > 100)
          infer_params.SAMPLES = 1;
          infer_params.ITERS = 2;
      else
          infer_params.SAMPLES = 3;
          infer_params.ITERS   = 3;
      end
      train_tas = infer_tas_indicators(train, model, tas_params, ...
                                       infer_params, 1);
      LL = 0;
      for m = 1:length(train_tas.pl)
          for i = 1:length(train_tas.pl{m})
              P2 = train_tas.pl{m}(i);
              if(train.l{m}(i)==2)
                  LL = LL + log(P2);
              else
                  LL = LL + log(1-P2);
              end
          end
      end
      score = LL;
      bic = discrim_bic_penalty(train, model);
  else  % GENERATIVE     P(T,R)
      LL = tas_data_ll_indicators(train, model);      
      bic = bic_penalty(train, model);
  end
  
  %% SCORE
  if(strcmp('bic',tas_params.structure_score))
      score = LL - bic;  
  elseif(strcmp('prior', tas_params.structure_score))
      score = LL + structure_prior(model, tas_params);
  else
      error('Unknown Structure Score Type!');
  end
  
  return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function logp = structure_prior(model, tas_params)
  N = sum(model.active_rs);
  mu    = tas_params.expected_num_rs;
  sigma = tas_params.stddev_num_rs;
  logp = -normlike([mu sigma], N);
  return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function bic = discrim_bic_penalty(train, model)

  % Number of Training Instances (for each family)
  M = length(train.name);
  M_L = 0;
  for m = 1:M
      N = size(train.b{m},1);
      M_L = M_L + N;
  end

  % Number of "Free Parameters"
  dim_R = sum((model.LK*model.QK-1)*model.active_rs + 1*(1- model.active_rs));  
  dim_L = (model.LK-1);
  dim_Q = (model.QK-1);
  dim_F = (model.FN + model.FN^2/2);

  % BIC Score
  bic = log(M)/2 * (dim_R + dim_L + dim_Q + dim_F);

  return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function bic = bic_penalty(train, model)

  % Number of Training Instances (for each family)
  M = length(train.name);
  M_R = 0; M_L = 0; M_Q = 0; M_F = 0;
  for m = 1:M
      N = size(train.b{m},1);
      J = size(train.f{m},1);
      M_R = M_R + N*J;
      M_L = M_L + N;
      M_Q = M_Q + J;
      M_F = M_F + J;
  end

  % Number of "Free Parameters"
  dim_R = sum((model.LK*model.QK-1)*model.active_rs + 1*(1- model.active_rs));  
  dim_L = (model.LK-1);
  dim_Q = (model.QK-1);
  dim_F = (model.FN + model.FN^2/2);

  % BIC Score
  bic = log(M_R)/2 * dim_R + log(M_L)/2 * dim_L + log(M_Q)/2 * dim_Q + log(M_F)/2 * dim_F;

  return
end
