function lp = log_gauss_pdf(D,mu,prec,logdet_prec)

dev = D-mu;
d = length(mu);
lp = -d/2*log(2*pi);
lp = lp + .5*logdet_prec;
temp = dev*prec;
lp = lp - .5 * sum(dev .* temp);
