function k = lmnrnd(logp, num)
% function k = lmnrnd(l, num)
%
%  Samples from a multinomial distribution
%   l is a log probability vector
%   num is the number of samples to draw (default = 1)
%

    % l is a row vector
    [l_max, m] = max(logp);
    p = exp(logp-l_max);
    % normalize to sum to 1 (and correct numerical issues)
    p = p/sum(p);
    
    % cumsum and find min
    if(num == 1)
        A = rand * ones(1,length(p));
        B = cumsum(p);
    else
        A = repmat(rand(num,1),1,length(p));
        B = repmat(cumsum(p),num,1);        
    end    
    D = (A < B)';    
    k = (length(p)+1) - sum(D);

end
