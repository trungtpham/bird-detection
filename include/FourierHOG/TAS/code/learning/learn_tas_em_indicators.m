function [model, exp_ss] = learn_tas_em_indicators(train, tas_params, ...
                                                  active_rs, epsilon, ...
                                                  initial_exp_ss)
% function model = learn_tas_em_indicators(train, tas_params)
%
%  Learns the parameters of a TAS model using incomplete data
%
    rand('state', 1);
    % Compute Relationship Variables
    if(~isfield(train,'r'))
        train = update_relationship_indicators(train);
    end
    
    % Set the l variable values
    train.l = train.truth_labels;
    F = cell2mat(train.f');
    if(size(F,1) > 20000)
        order = randperm(size(F,1));
        F = F(order(1:20000), :);
    end
    
    % Cardinalities
    model.QK = tas_params.QK;
    model.LK = 2;
    model.RK = size(train.r{1},3);
    model.FN = size(train.f{1},2);
    fprintf('Learning TAS model with:\n');
    fprintf('QK = %d, LK = %d, RK = %d, FN = %d\n', model.QK, model.LK, ...
            model.RK, model.FN);
    model.region_f_model = 'gaussian';
    model.active_rs            = active_rs;
    
    % Initialize
    bestLL = -inf;
    best_model = [];
    for restarts = 1:tas_params.EM_RESTARTS
        model = random_tas_params_indicators(model, F);
        
        LL    = -1e10;
        oldLL = -2e10;
        iter  = 0;
        
        while((LL-oldLL)/abs(oldLL) > epsilon)
            iter = iter + 1;
            
            
            % E STEP
            % Get Expected Sufficient Statistics
            fprintf('E-STEP...');
            start = now;            
            if(exist('initial_exp_ss','var') && (restarts==1) && (iter==1))
                exp_ss = initial_exp_ss;
            else
                exp_ss = get_tas_exp_ss_indicators(train, model, tas_params);
            end
            fprintf('%g min\n', 24*60*(now-start));
            
            % M STEP
            % Estimate Parameters
            fprintf('M-STEP...');
            start = now;            
            model = estimate_tas_parameters_indicators(train, exp_ss, ...
                                                       model, tas_params);
            fprintf('%g min\n', 24*60*(now-start));
            
            
            % Compute New Likelihood
            if(mod(iter,10)==1)
                oldLL = LL;
                fprintf('***************************\n');
                start = now;            
                LL = tas_data_ll_indicators(train, model);
                fprintf('\t\t\t  ');
                fprintf('     %g', model.active_rs);
                fprintf('\n');
                for q = 1:model.QK
                    fprintf('Q=%g\t%0.2f\t', q, model.theta_q(q));
                    fprintf('P(r|l=1,q) = ', q);
                    for k = 1:model.RK
                        if(model.active_rs(k))
                            fprintf('%0.3f ', model.theta_r(k,2,2,q));
                        else
                            fprintf('%0.3f ', model.theta_r(k,2,1,1));
                        end
                    end
                    fprintf('\n');
                end
                fprintf('EM #%d: Data LL(%d) = %f\n', restarts, iter, LL);
                fprintf('LL took %g min\n', 24*60*(now-start));
                fprintf('***************************\n');
            end
        end
        
        fprintf('***************************\n');
        fprintf('***************************\n');
        fprintf('***************************\n');
        fprintf('P(Q) :');
        fprintf('%0.3f ', model.theta_q(:));
        fprintf('\n');
        if(isfield(model, 'mu_f'))
            fprintf('MU(1) : ');
            fprintf('%0.3f ', model.mu_f(1,:));
            fprintf('\nSTD : ');
            for q = 1:model.QK
                fprintf('%0.3f ', sqrt(max(eig(model.cov_f(:,:, q)))));
            end            
            fprintf('\n');
        end        
        fprintf('***************************\n');
        fprintf('***************************\n');
        fprintf('***************************\n');
        
        if(LL > bestLL)
            bestLL = LL;
            best_model = model;
            best_exp_ss = exp_ss;
        end
    end
    
    model   = best_model;
    exp_ss  = best_exp_ss;
    fprintf('EM: Choosing Model with Data LL = %0.3f\n', bestLL);
    
    
    return
end
    
