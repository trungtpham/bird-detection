function ins = update_relationship_indicators(ins)
%  function ins = update_relationships(ins)
%
    fprintf('Observing Rs...');
    ins.r = cell(1,length(ins.b));
    M = length(ins.b);
    NR = 25;
    
    for m = 1:M
        fprintf('.');
        N = size(ins.b{m},1); % Number of detections
        J = size(ins.f{m},1); % Number of regions
        ins.r{m} = ones(N,J,NR);        
        for r= 1:NR
            for i = 1:N
                j = find_regions_with_relationship(ins.ci{m}(i,:), ...
                                                   ins.delta{m}(i,:), ...
                                                   ins.cj{m}, r, ...
                                                   ins.image_size{m});
                ins.r{m}(i,j,r) = 2;
            end              
        end
    end 
    
    % Precompute R sums
    for m = 1:M
        ins.R_sumOveri{m} = sum(ins.r{m},1);
        ins.R_sumOverj{m} = sum(ins.r{m},2);
    end
    
    fprintf('Done\n');
    return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function j = get_j_for_p(p, cj, J, image_size)    
  if((p(1) < 1) | (p(1) > image_size(2)) | (p(2) < 1) | (p(2) > image_size(1)))
      j = [];
  else
      d = sum((repmat(p,J,1)-cj).^2, 2);
      [val j] = min(d);
  end
  return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function j = get_j_near_p(p, cj, J, T)    
  d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
  j = find(d < T);  
  return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function j = find_regions_with_relationship(ci, delta, cj, r, image_size)

%  T = 2*sqrt(sum(delta.^2));
  T = 50;
  J = size(cj,1);
  switch(r)
   case 1,  % IN
    p = ci;
    j = get_j_for_p(p,cj,J,image_size);
   case 2,  % NEAR-NEAR
    p = ci;
    j = get_j_near_p(p,cj,J,T);
   case 3,  % ABOVE X 2
    p = ci + (delta .* [0 -2]);
    j = get_j_for_p(p, cj, J, image_size);
   case 4,  % BELOW X 2
    p = ci + (delta .* [0 +2]);
    j = get_j_for_p(p,cj,J,image_size);
   case 5,  % LEFT X 2
    p = ci + (delta .* [-2 0]);
    j = get_j_for_p(p,cj,J,image_size);
   case 6,  % RIGHT X 2
    p = ci + (delta .* [+2 0]);
    j = get_j_for_p(p,cj,J,image_size);
   case 7,  % ABOVE_LEFT X 2
    p = ci + (delta .* (sqrt(2)*[-1 -1]));
    j = get_j_for_p(p,cj,J,image_size);
   case 8,  % ABOVE_RIGHT
    p = ci + (delta .* (sqrt(2)*[+1 -1]));
    j = get_j_for_p(p,cj,J,image_size);
   case 9,  % BELOW_LEFT
    p = ci + (delta .* (sqrt(2)*[-1 +1]));
    j = get_j_for_p(p,cj,J,image_size);
   case 10,  % BELOW_RIGHT
    p = ci + (delta .* (sqrt(2)*[+1 +1]));
    j = get_j_for_p(p,cj,J,image_size);    
   case 11,  % ABOVE X 4
    p = ci + (delta .* [0 -4]);
    j = get_j_for_p(p,cj,J,image_size);
   case 12,  % BELOW X 4
    p = ci + (delta .* [0 +4]);
    j = get_j_for_p(p,cj,J,image_size);
   case 13,  % LEFT X 4
    p = ci + (delta .* [-4 0]);
    j = get_j_for_p(p,cj,J,image_size);
   case 14,  % RIGHT X 4
    p = ci + (delta .* [+4 0]);
    j = get_j_for_p(p,cj,J,image_size);
   case 15,  % ABOVE_LEFT X 4
    p = ci + (delta .* (sqrt(8)*[-1 -1]));
    j = get_j_for_p(p,cj,J,image_size);
   case 16,  % ABOVE_RIGHT X 4
    p = ci + (delta .* (sqrt(8)*[+1 -1]));
    j = get_j_for_p(p,cj,J,image_size);
   case 17,  % BELOW_LEFT X 4
    p = ci + (delta .* (sqrt(8)*[-1 +1]));
    j = get_j_for_p(p,cj,J,image_size);
   case 18,  % BELOW RIGHT X 4
    p = ci + (delta .* (sqrt(8)*[+1 +1]));
    j = get_j_for_p(p,cj,J,image_size);
   
   % COMBOS
   case 19,  % Any of 3,4,5,6
    j = find_regions_with_relationship(ci, delta, cj, 3, image_size);
    j = union(j,find_regions_with_relationship(ci, delta, cj, 4, image_size));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 5, image_size));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 6, image_size));
   case 20,  % Any of 7,8,9,10
    j = find_regions_with_relationship(ci, delta, cj, 7, image_size);
    j = union(j,find_regions_with_relationship(ci, delta, cj, 8, image_size));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 9, image_size));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 10, image_size));   
   case 21,  % Any of 11,12,13,14
    j = find_regions_with_relationship(ci, delta, cj, 11, image_size);
    j = union(j,find_regions_with_relationship(ci, delta, cj, 12, image_size));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 13, image_size));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 14, image_size));
   case 22,  % Any of 15,16,17,18
    j = find_regions_with_relationship(ci, delta, cj, 15, image_size);
    j = union(j,find_regions_with_relationship(ci, delta, cj, 16, image_size));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 17, image_size));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 18, image_size));
   case 23,  % LEFT OR RIGHT X 2
    j = find_regions_with_relationship(ci, delta, cj, 5, image_size);
    j = union(j,find_regions_with_relationship(ci, delta, cj, 6, image_size));
   case 24,  % LERT OR RIGHT X 4
    j = find_regions_with_relationship(ci, delta, cj, 13, image_size);
    j = union(j,find_regions_with_relationship(ci, delta, cj, 14, image_size));
   case 25,  % LERT OR RIGHT X 2 or 4
    j = find_regions_with_relationship(ci, delta, cj, 5, image_size);
    j = union(j,find_regions_with_relationship(ci, delta, cj, 6, image_size));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 13, image_size));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 14, image_size));
   otherwise,
    error(sprintf('Unknown R = %d', r));
  end
  
  return
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function j = old_find_regions_with_relationship(ci, delta, cj, r, image_size)

%  T = 2*sqrt(sum(delta.^2));
  T = 50;
  J = size(cj,1);
  switch(r)
   case 1,  % IN
    p = ci;
    d = sum((repmat(p,J,1)-cj).^2, 2);
    [val j] = min(d);
   case 2,  % NEAR-NEAR
    p = ci;
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    j = find(d < T);
   case 3,  % ABOVE X 2
    p = ci + (delta .* [0 -2]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    [val j] = min(d);
   case 4,  % ABOVE X 2
    p = ci + (delta .* [0 -2]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    j = find(d < T);
   case 5,  % BELOW X 2
    p = ci + (delta .* [0 +2]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    [val j] = min(d);
   case 6,  % BELOW X 2
    p = ci + (delta .* [0 +2]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    j = find(d < T);
   case 7,  % LEFT X 2
    p = ci + (delta .* [-2 0]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    [val j] = min(d);
   case 8,  % LEFT X 2
    p = ci + (delta .* [-2 0]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    j = find(d < T);
   case 9,  % RIGHT X 2
    p = ci + (delta .* [+2 0]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    [val j] = min(d);
   case 10,  % RIGHT X 2
    p = ci + (delta .* [+2 0]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    j = find(d < T);
   case 11,  % Any of 3,5,7,9
    j = find_regions_with_relationship(ci, delta, cj, 3);
    j = union(j,find_regions_with_relationship(ci, delta, cj, 5));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 7));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 9));
   case 12,  % Any of 4,6,8,10
    j = find_regions_with_relationship(ci, delta, cj, 4);
    j = union(j,find_regions_with_relationship(ci, delta, cj, 6));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 8));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 10));
   case 13,  % ABOVE_LEFT X 2
    p = ci + (delta .* (sqrt(2)*[-1 -1]));
    d = sum((repmat(p,J,1)-cj).^2, 2);
    [val j] = min(d);
   case 14,  % ABOVE_RIGHT
    p = ci + (delta .* (sqrt(2)*[+1 -1]));
    d = sum((repmat(p,J,1)-cj).^2, 2);
    [val j] = min(d);
   case 15,  % BELOW_LEFT
    p = ci + (delta .* (sqrt(2)*[-1 +1]));
    d = sum((repmat(p,J,1)-cj).^2, 2);
    [val j] = min(d);
   case 16,  % BELOW_RIGHT
    p = ci + (delta .* (sqrt(2)*[+1 +1]));
    d = sum((repmat(p,J,1)-cj).^2, 2);
    [val j] = min(d);
   case 17,  % ABOVE X 4
    p = ci + (delta .* [0 -4]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    j = find(d < T);
   case 18,  % BELOW X 4
    p = ci + (delta .* [0 +4]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    j = find(d < T);
   case 19,  % LEFT X 4
    p = ci + (delta .* [-4 0]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    j = find(d < T);
   case 20,  % RIGHT X 4
    p = ci + (delta .* [+4 0]);
    d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
    j = find(d < T);
   case 21,  % Any of 17,18,19,20
    j = find_regions_with_relationship(ci, delta, cj, 17);
    j = union(j,find_regions_with_relationship(ci, delta, cj, 18));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 19));
    j = union(j,find_regions_with_relationship(ci, delta, cj, 20));
   case 22,  % ABOVE X 2 or OFF
    p = ci + (delta .* [0 -2]);
    if((p(1) < 1) | (p(1) > image_size(2)) | (p(2) < 1) | (p(2) > image_size(1)))
        j = [];
    else
        d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
        [val j] = min(d);
    end
   case 23,  % BELOW X 2
    p = ci + (delta .* [0 +2]);
    if((p(1) < 1) | (p(1) > image_size(2)) | (p(2) < 1) | (p(2) > image_size(1)))
        j = [];
    else
        d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
        [val j] = min(d);
    end
   case 24,  % LEFT X 2
    p = ci + (delta .* [-2 0]);
    if((p(1) < 1) | (p(1) > image_size(2)) | (p(2) < 1) | (p(2) > image_size(1)))
        j = [];
    else
        d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
        [val j] = min(d);
    end
   case 25,  % RIGHT X 2
    p = ci + (delta .* [+2 0]);
    if((p(1) < 1) | (p(1) > image_size(2)) | (p(2) < 1) | (p(2) > image_size(1)))
        j = [];
    else
        d = sqrt(sum((repmat(p,J,1)-cj).^2, 2));
        [val j] = min(d);
    end
   otherwise,
    error(sprintf('Unknown R = %d', r));
  end
  
  return
end


