function model = estimate_tas_parameters_indicators(train, exp_ss, model, tas_params)
% function model = estimate_tas_parameters(train, exp_ss, model)
%
%  Estimate the parameters for a TAS model using expected
%  sufficient statistics
%
%  model: TAS model format
%     - model.w_l            = Logistic Regression Parameters for P(l | b)
%     - model.theta_q(i)     = Multinomial parameters for P(q = i)
%     - model.theta_r(k,i,l,q) = Multinomial parameters for P(r_k=i|l,q)
%     - model.theta_w(i,j,k) = Multinomial parameters for P(w_j=i|q=k)

    pseudocounts = 1;
    if(~isfield(model, 'QK'))
        error('Cardinality of Q must be defined in variable QK');
    end
    if(~isfield(model, 'LK'))
        error('Cardinality of L must be defined in variable LK');
    end
    if(~isfield(model, 'RK'))
        error('Cardinality of R must be defined in variable RK');
    end
    if(~isfield(model, 'FN'))
        error('Length of region features musted be defined in variable FN');
    end

    % CPD for l
    if(~isfield(model, 'w_l'))
        % Weights for logistic P(l | detection_features)
        % P(l_i | b_i) = 1 / (1 + exp(-w_l' * [1 b_i]))
        X = cell2mat(train.b');
        Y = cell2mat(train.truth_labels') - 1;
        [model.w_l Ib Vb] = logitfit(Y,X);
    end
    
    % P(q = i)
    model.theta_q = exp_ss.q_counts + pseudocounts;
    model.theta_q = (1.0 / sum(model.theta_q)) * model.theta_q;
    
    % P(f | q)
    if(strcmp('gaussian', model.region_f_model))
        model.diag_cov = tas_params.diag_cov;
        for q = 1:model.QK
            model.mu_f(:,q) = exp_ss.f_mu(:,q) ./ exp_ss.f_counts(q);
            if(model.diag_cov)
                model.cov_diag_f(:,q) = exp_ss.f_cov_diag(:,q) ./ exp_ss.f_counts(q) ...
                    - model.mu_f(:,q) .* model.mu_f(:,q) + 1e-6;
                model.cov_f(:,:,q) = diag(model.cov_diag_f(:,q));
                model.prec_diag_f(:,:,q) = 1 ./ model.cov_diag_f(:,q);
            else
                model.cov_f(:,:,q) = exp_ss.f_cov(:,:,q) ./ exp_ss.f_counts(q) ...
                    - model.mu_f(:,q) * model.mu_f(:,q)' + 1e-6 * eye(model.FN);
            end
            model.prec_f(:,:,q) = inv(model.cov_f(:,:,q));
            model.logdet_prec_f(q) = logdet(model.prec_f(:,:,q));
        end
    else
        error('Only Gaussian');
    end
    
    % P(r | l,q)
    model.theta_r = exp_ss.r_counts + pseudocounts;
    for k = 1:model.RK
        if(model.active_rs(k))
            % R is "active", parameters depend on l and q
            for l = 1:model.LK
                for q = 1:model.QK
                    model.theta_r(k,:,l,q) = (1.0 / sum(model.theta_r(k,:,l,q))) ...
                        * model.theta_r(k,:,l,q);
                end
            end
        else
            % R is "inactive", parameters are independent of l and q
            temp = sum(sum(model.theta_r(k,:,:,:),4), 3);
            model.theta_r(k,:,:,:) = -inf;
            model.theta_r(k,:,1,1) = (1.0 / sum(temp)) * temp;
        end
    end
    
    return
end
    
