function labels = get_truth_labels(cand_dets, gt, THRESHOLD)

    % Walk through all images
    fprintf('Getting truth labels: ');
    for m = 1:length(cand_dets)
        fprintf(' %d ', m);
        if(m > length(gt)), continue; end;
        available_gt = ones(size(gt{m},1),1);
        
        % Walk through all pairs of dets and gts
        cost = [];
        I = size(cand_dets{m},1);
        G = size(gt{m},1);
        for i = 1:I
            candbox = cand_dets{m}(i,:);
            for g = 1:G
                gtbox = gt{m}(g,:);
                overlap = detection_overlap(candbox,gtbox);
                cost(i,g) = 1-overlap;
            end
        end
                
        labels{m} = ones(I,1);
        
        % Pick off the best scores until no GTs are left
        if(size(cost,1) > 0 && size(cost,2) > 0)
            for g = 1:G
                [mincost i] = min(cost(:,g));
                T = 1 - THRESHOLD; 
                
                if(mincost < T)
                    labels{m}(i) = 2;
                    cost(:,g) = inf;
                end            
            end
        end
        
    end
    fprintf('\n');
    
    return
end
