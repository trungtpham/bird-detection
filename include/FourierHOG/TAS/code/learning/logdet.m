function y = logdet(A)
% log(det(A)) where A is positive-definite.

U = chol(A);
y = 2*sum(log(diag(U)));
