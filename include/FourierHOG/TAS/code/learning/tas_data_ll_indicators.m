function ll = tas_data_ll_indicators(ins, model)
%
%  Computes P(l,r,f | b) of the data
%
%

M = length(ins.l);
ll = 0;
log_theta_r = log(model.theta_r+eps);
log_theta_q = log(model.theta_q+eps);

for m = 1:M
    this_ll = 0;

    J = size(ins.f{m},1);
    N = size(ins.b{m},1);
    if(N==0), continue; end;
    R = ins.r{m};
    L = ins.l{m};

    % P(l_i | b_i)
    for i = 1:N
        this_ll = this_ll + label_ll(ins.l{m}(i), ins.b{m}(i,:), ...
                                     model);
    end 

    r = zeros(N,1);
    l = zeros(N,1);
    for j = 1:J
        this_ll_qsum = -inf;
        f = ins.f{m}(j,:)';
        for q = 1:model.QK
            % P(Q = q)
            this_ll_q = log_theta_q(q);
            
            % P(F | Q = q)
            if(model.diag_cov)
                mu = model.mu_f(:,q);
                lp = -length(mu)/2*log(2*pi) + 0.5*model.logdet_prec_f(q) - 0.5 * sum((f-mu).^2.*model.prec_diag_f(:,q));
%                lp2 = log_gauss_pdf(ins.f{m}(j,:), model.mu_f(:,q)', ...
%                                    model.prec_f(:,:,q), ...
%                                    model.logdet_prec_f(q));
%                if(abs(lp-lp2) > 1e-6), error('hello'); end;
            else
                lp = log_gauss_pdf(ins.f{m}(j,:), model.mu_f(:,q)', ...
                                   model.prec_f(:,:,q), ...
                                   model.logdet_prec_f(q));
            end
            this_ll_q = this_ll_q + lp;
            
            % P(R = r | L = l, Q = q)
            for k = 1:model.RK
                if(model.active_rs(k))
                    inds = (q-1)*model.RK*2*2 + (L-1)*model.RK*2 + ...
                           (R(:,j,k)-1)*model.RK + k;
                else
                    inds = (R(:,j,k)-1)*model.RK + k;
                end
                this_ll_q = this_ll_q + sum(log_theta_r(inds));
            end
            
            this_ll_qsum = addlog(this_ll_qsum, this_ll_q);
        end
        this_ll = this_ll + this_ll_qsum;
    end    
    
    ll = ll + this_ll;            
end

