function exp_ss = get_tas_exp_ss_indicators(ins, model, tas_params)
% function exp_ss = get_tas_exp_ss(ins, model, tas_params)
%
%   Compute the expected sufficient statistics.

    % Given all the data, we compute P(q_j) for all j
    % P(q_j | l, r, w)
    %   ~ P(q_j, r_j, w_j | l)
    %   = P(q_j) prod_d P(w_jd | q_j) prod_i P(r_ij | q_j, l_i)
    exp_ss.q_counts = zeros(model.QK,1);
    exp_ss.r_counts = zeros(model.RK,2,model.LK,model.QK);
    r_counts  = zeros(model.QK,2,model.LK,model.RK);
    exp_ss.f_counts = zeros(1,model.QK);
    exp_ss.f_mu       = zeros(model.FN, model.QK);
    exp_ss.f_cov      = zeros(model.FN, model.FN, model.QK);
    exp_ss.f_cov_diag = zeros(model.FN, model.QK);
    for m = 1:length(ins.l)
               
        J = size(ins.f{m},1);
        N = size(ins.b{m},1);
        if(N == 0), continue; end; % APPROXIMATION (DROPS IMAGES WITHOUT CANDIDATES)
                
        % P(q | w,r,l)
        if(tas_params.pre_cluster)
            logP = log_appearance_q(ins, m, model);            
        else    
            logP = log_posterior_q_indicators(ins, m, model);
        end

        L = ins.l{m};            
        R = ins.r{m};
        for j = 1:J
            %%%%%%% COMPUTE PROBABILITIES
            % P(q | w,r,l)
            P = exp(logP(j,:))';
            P = P ./ sum(P);

            %%%%% ACCRUE COUNTS
            % Q COUNTS
            exp_ss.q_counts = exp_ss.q_counts + P;
            
            % F SUFF_STATS
            if(strcmp('gaussian', model.region_f_model))
                f   = ins.f{m}(j,:)';
                ff  = f.^2;
                fft = f*f';
                for q = 1:model.QK
                    exp_ss.f_counts(q)     = exp_ss.f_counts(q)  + P(q);
                    exp_ss.f_mu(:,q)       = exp_ss.f_mu(:,q)    + P(q) * f;
                    if(tas_params.diag_cov)
                        exp_ss.f_cov_diag(:,q) = exp_ss.f_cov_diag(:,q) + P(q) * ff;
                    else
                        exp_ss.f_cov(:,:,q)    = exp_ss.f_cov(:,:,q) + P(q) * fft;
                    end
                end
            else
                error('only gaussian');
            end
            
            % R COUNTS
            if(N > 0)
                for k = 1:model.RK
                    rcnts = zeros(2, model.LK);
                    for i = 1:N
                        rcnts(R(i,j,k),L(i)) = rcnts(R(i,j,k),L(i)) + 1;
                    end
                    if(rcnts(1,1)>0) r_counts(:,1,1,k) = r_counts(:,1,1,k) + rcnts(1,1)*P; end;
                    if(rcnts(1,2)>0) r_counts(:,1,2,k) = r_counts(:,1,2,k) + rcnts(1,2)*P; end;
                    if(rcnts(2,1)>0) r_counts(:,2,1,k) = r_counts(:,2,1,k) + rcnts(2,1)*P; end;
                    if(rcnts(2,2)>0) r_counts(:,2,2,k) = r_counts(:,2,2,k) + rcnts(2,2)*P; end;
                end
            end
        end

    end

    exp_ss.r_counts = permute(r_counts, [4 2 3 1]);
    
    return
end
    
