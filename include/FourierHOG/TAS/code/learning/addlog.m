function logxpy = addlog(x,y)
% function logxpy = addlog(x,y)

  if( x == -inf) logxpy = y; return; end;
  if( y == -inf) logxpy = x; return; end;

  if( x > y )
    z = y - x;
  else
    z = x - y;
    x = y;
  end
  logxpy = x + log(1 + exp(z));
  
  return
end
