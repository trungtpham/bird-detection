function [ins, imgset] = load_tas_data(class_name, data_dir, params, ...
                                       image_indices)

    % Directories
    imagedir   = [data_dir, '/Images'];
    cand_dir   = [data_dir, '/Candidates'];
    region_dir = [data_dir, '/Regions'];
    gt_dir     = [data_dir, '/Groundtruth'];

    % Constants
    scoretype = 'margin';

    % Files
    D = dir([imagedir, '/*jpg']);    
    if(~exist('image_indices')), image_indices=1:length(D); end;
    
    M = length(image_indices);
    for m = 1:M
        
        id = image_indices(m);
                
        fprintf('Loading instance #%d\n', m);
        % Image
        image_filename = sprintf('%s/%s', imagedir, D(id).name);
        [d ins.name{m} e] = fileparts(image_filename);
        ins.image_filename{m} = image_filename;
        fprintf('\tIMAGE: %s\n', image_filename);
        I = imread(image_filename);
        ins.image_size{m} = size(I);
        ins.seg_filename{m} = sprintf('%s/%s_S.mat', region_dir, ins.name{m});
        fprintf('\tSEG: %s\n', ins.seg_filename{m});
        load(ins.seg_filename{m});
        ins.segs{m} = S; 
        

        % STEP 2: Context Cues
        fprintf('\tREGION FEATURES\n');
        ftr_filename = sprintf('%s/%s_FTR.mat', region_dir, ins.name{m});
        load(ftr_filename);
        ins.f{m} = data;
        % Check it
        if(size(ins.f{m},1) ~= length(unique(ins.segs{m})))
            error('The number of segment features does not match the number of segments');                
        end   
        % Precompute Region Centroids
        J = size(ins.f{m},1);
        for j = 1:J
            jinds = find(ins.segs{m}==j);
            [SI SJ] = ind2sub(size(ins.segs{m}), jinds);
            ins.cj{m}(j,:) = mean([SJ SI]);            
        end                
        
        % STEP 3: GROUNDTRUTH
        fprintf('\tGROUNDTRUTH\n');
        gt_filename = sprintf('%s/%s/%s.gt', gt_dir, class_name, ins.name{m});
        ins.gt{m} = load(gt_filename);
        
        % STEP 4: DETECTION CANDIDATES
        fprintf('\tCANDIDATES: ');
        ins.scores{m} = []; ins.b{m} = []; ins.dets{m} = [];
        ins.ci{m}    = [];  ins.delta{m} = [];
        cand_filename = sprintf('%s/%s/%s.cand', cand_dir, class_name, ...
                                ins.name{m});
        % Only cands above threshold
        cands = load(cand_filename);                                
        good_cands = find(cands(:,5)>params.cand_threshold);
        cands = cands(good_cands,:);
        if(size(cands,1) > 0)
            ins.scores{m} = cands(:,5);
            R = -log(1./ins.scores{m} - 1); % invert this: P = 1/1+exp(-R)
            ins.b{m} = [ones(size(cands,1),1) R];
            ins.dets{m}   = [cands(:,1:2) cands(:,1:2)+cands(:,3:4)];
            ins.ci{m}    = round((ins.dets{m}(:,1:2) + ins.dets{m}(:,3:4))/2);
            ins.delta{m} = cands(:,3:4);
        end        
        ins.N{m} = size(ins.dets{m},1);
        fprintf('Loaded %d candidates\n', ins.N{m});
    end
        
    if(isfield(ins, 'gt') && isfield(ins, 'dets'))
        ins.positive_count = size(cell2mat(ins.gt'), 1);
        ins.truth_labels = get_truth_labels(ins.dets, ins.gt, ...
                                            params.truth_threshold);
    end

    
     return
 end
    
