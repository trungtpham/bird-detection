function ll = label_ll(l, b, model);
    p  = 1 ./ (1 + exp(-b*model.w_l));
    if(l == 2)
        ll = log(p+eps);
    else
        ll = log(1-p+eps);
    end    
    return
end
