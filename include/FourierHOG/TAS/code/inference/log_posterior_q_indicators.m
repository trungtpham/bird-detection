function logP = log_posterior_q_indicators(ins, m, model, logPFjgivenQj)
%  function logP = log_posterior_q(ins, m, N, model, logPFjgivenQj)

    fjlookup = false;
    if(exist('logPFjgivenQj', 'var')), fjlookup = true; end;    
    log_theta_r = log(model.theta_r+eps);
    J = size(ins.r{m},2);
    L = ins.l{m};
    f = ins.f{m};
    r = ins.r{m};
    
    % P(q)
    logPq  = log(model.theta_q);

    logPf = zeros(model.QK, 1);
    logP  = zeros(J, model.QK);
    all_inds = zeros(length(L), model.QK);
    
    % Compute table of P(f|q)'s
    if(fjlookup)
        logPfj = logPFjgivenQj;
    else
        logPfj = zeros(model.QK, J);
        for q = 1:model.QK
            mu = model.mu_f(:,q)';
            prec = model.prec_f(:,:,q);
            logdet_prec = model.logdet_prec_f(q);
            for j = 1:J
                % P(f_j | q_j) - Log Gauss PDF
                dev = f(j,:)-mu;
                d = length(mu);
                lp = -d/2*log(2*pi);
                lp = lp + .5*logdet_prec;
                    temp = dev*prec;
                    logPfj(q,j) = lp - .5 * sum(dev .* temp);
            end                    
        end
    end

    for k = 1:model.RK
        if(model.active_rs(k))
            for q = 1:model.QK
                a = log_theta_r(k,:,:,q);
                ltr(q,:,k) = a(:)';
            end
        end
    end
    
    % Loop through regions
    sumR = ins.R_sumOveri{m};
    NR = size(r,1);
    for j = 1:J
        logPf = logPfj(:,j);
    
        logPrs_active   = zeros(model.QK,1);
        logPrs_inactive = 0;
        for k = 1:model.RK
            % P(r|l,q)
            %  - model.theta_r(i,j,k) = Multinomial parameters for  P(r_i=1|l=j,q=k)
            if(model.active_rs(k))
                R = r(:,j,k);
                % "Active" R
                rlcounts = zeros(2, model.LK);
                for i = 1:length(L)
                    rlcounts(R(i),L(i)) = rlcounts(R(i),L(i)) + 1;
                end
                rlcounts = rlcounts(:);               
                logPrs_active = logPrs_active + ltr(:,:,k) * rlcounts;
            else
                % "Inactive" R
                NR2 = sumR(1,j,k)-NR;
                NR1 = NR-NR2;
                logPrs_inactive = logPrs_inactive + log_theta_r(k,1,1,1) ...
                    * NR1 + log_theta_r(k,2,1,1) * NR2;
            end
        end
        logPrs = logPrs_active+logPrs_inactive;
        
        % Compute P(q | l,r,w)
        logPj = logPq + logPf + logPrs;
        logPj = logPj - max(logPj);
        logP(j,:) = logPj';
    end
    
    return
end
    
