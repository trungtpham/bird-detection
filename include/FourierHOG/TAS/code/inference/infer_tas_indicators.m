function [ins samples] = infer_tas_indicators(ins, model, tas_params, ...
                                              INFER, silent)

    rand('state', 1);
    if(~exist('silent','var')), silent = 0; end;
    if(~isfield(ins,'r'))
        ins = update_relationship_indicators(ins);
    end
        
    % P(L | B)
    if(~silent), fprintf('Precomputing P(L)\n'); end;
    M = length(ins.f);
    logPriorL = cell(M,1);
    for m = 1:M
        N = size(ins.b{m},1);
        if(N == 0), continue; end;
        for i = 1:size(ins.b{m},1)            
            for l = 1:model.LK
                logPriorL{m}(i,l) = label_ll(l, ins.b{m}(i,:), model);
            end    
        end    
    end

    % Precompute all P(F|Q)'s
    if(~silent), fprintf('Precomputing P(F|Q)\n'); end;
    logPFjgivenQj = cell(M,1);
    for m = 1:M
        N = size(ins.b{m},1);
        if(N == 0), continue; end;
        logPFjgivenQj{m} = zeros(model.QK,size(ins.f{m},1));
        for j = 1:size(ins.f{m},1)
            for q = 1:model.QK
                logPFjgivenQj{m}(q,j) = log_gauss_pdf(ins.f{m}(j,:), model.mu_f(:,q)', ...
                                                      model.prec_f(:,:,q), ...
                                                      model.logdet_prec_f(q));
            end
        end
    end
    
    % Loop over images
    for m = 1:M
        if(~silent), fprintf('Image #%d\n', m); end;

        N = size(ins.b{m},1);
        J = size(ins.f{m},1);
        if(N == 0), continue; end;

        infer_samples = INFER.SAMPLES;
        infer_iters   = INFER.ITERS;
        pl{m} = zeros(N, 1);
        for s = 1:infer_samples
            if(~silent), fprintf('Infer Sample #%d\n', s); end;
            ins = infer_init(ins, model, logPriorL, m, INFER.INIT);
            samples{s,1}.q{m}  = ins.q{m};
            samples{s,1}.l{m}  = ins.l{m};
            for t = 1:infer_iters
                
                % Resample all L's
                % P(L_i | Q, R, W)
                logP = log_posterior_l_indicators(ins, m, model, logPriorL);
                for i = 1:N
                    P = exp(logP(i,:)) / (sum(exp(logP(i,:))));
                    ins.l{m}(i)  = (rand(1) < P(2)) + 1;
                    ins.pl{m}(i) = P(2);
                end

                % Resample all Q's
                % P(Q_j | L, R, W)
                if(tas_params.pre_cluster)
                    logP = log_appearance_q(ins, m, model);
                else
                    logP = log_posterior_q_indicators(ins, m, model, ...
                                                      logPFjgivenQj{m});                           
                end
                
                for j = 1:J
                    % Sample it
                    ins.q{m}(j,1) = lmnrnd(logP(j,:), 1);
                end
                                
                if(t == infer_iters)
                    this_image_ins.l{1} = ins.l{m};
                    this_image_ins.q{1} = ins.q{m};
                    this_image_ins.b{1} = ins.b{m};
                    this_image_ins.r{1} = ins.r{m};
                    this_image_ins.f{1} = ins.f{m};
                end                
                
                if(t==1)
                    samples{s,2}.q{m}  = ins.q{m};
                    samples{s,2}.l{m}  = ins.l{m};
                    samples{s,2}.pl{m} = ins.pl{m};
                end
                    
            end
            pl{m} = pl{m} + ins.pl{m};
            
            % Gather info from the sample
            samples{s,3}.q{m}  = ins.q{m};
            samples{s,3}.l{m}  = ins.l{m};
            samples{s,3}.pl{m} = ins.pl{m};
        end        
        
        ins.pl{m} = pl{m} / infer_samples;;
    end
        
    return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ins = infer_init(ins, model, logPriorL, m, INIT)

    if(strcmp('sample',INIT))
        % Initialize L's
        for i = 1:size(ins.b{m},1)            
            P = exp(logPriorL{m}(i,:)) / (sum(exp(logPriorL{m}(i,:))));
            ins.l{m}(i,1) = (rand(1) < P(2)) + 1;
            ins.pl{m}(i,1) = P(2);
        end        
        
        % Initialize Q's
        if(strcmp('visual_words', model.region_f_model))
            for j = 1:size(ins.w{m},1)
                lp = log(model.theta_w(ins.w{m}(j),1,:));
                lp = (lp(:) + log(model.theta_q(:)))';
                ins.q{m}(j,1) = lmnrnd(lp,1);
            end            
        elseif(strcmp('gaussian', model.region_f_model))
            for j = 1:size(ins.f{m},1)
                for q = 1:model.QK
                    logPf(q) = log_gauss_pdf(ins.f{m}(j,:), model.mu_f(:,q)', ...
                                             model.prec_f(:,:,q), ...
                                             model.logdet_prec_f(q));
                end
                lp = logPf + log(model.theta_q');
                ind = lmnrnd(lp,1);
                ins.q{m}(j,1) = ind;
            end            
        else
            error(sprintf('UKNOWN REGION FEATURE MODEL: %s', ...
                          model.region_f_model));
        end        
    elseif(strcmp('rand',INIT))
        ins.pl{m} = zeros(size(ins.b{m},1),1);
        ins.l{m}  = zeros(size(ins.b{m},1),1);
        ins.q{m}  = zeros(size(ins.f{m},1),1);
        for i = 1:size(ins.b{m},1)
            ins.l{m}(i) = ceil(rand*model.LK);
        end        
        for j = 1:size(ins.f{m},1)
            ins.q{m}(j) = ceil(rand*model.QK);
        end        
    else
        error('UNKNOWN INIT');
    end
    
    return
end
        
