function logP = log_posterior_l_indicators(ins, m, model, logPriorL)
%  function logP = log_posterior_l(ins, m, J, model, logPriorL)

    log_theta_r = log(model.theta_r+eps);
    N = size(ins.r{m},1);
    if(N == 0), logP = []; return; end;
    Q = ins.q{m};
    r = ins.r{m};

    for k = 1:model.RK
        if(model.active_rs(k))
            for l = 1:model.LK
                a = log_theta_r(k,:,l,:);
                ltr(l,:,k) = a(:)';
            end
        end
    end        
    
    my_outer_inds = (Q-1)' .* (model.LK * model.RK);
    logPls = logPriorL{m}';    
    sumR = ins.R_sumOverj{m};
    NR = size(r,2);
    logPrs_inactive = 0;
    logPrs = zeros(2,N);
    for i = 1:N
        logPrs_active   = zeros(2,1);
        logPrs_inactive = 0;
        for k = 1:model.RK
            % P(r|l,q)
            if(model.active_rs(k))
                R = r(i,:,k);
                rqcounts = zeros(2, model.QK);
                for j = 1:length(Q)
                    rqcounts(R(j),Q(j)) = rqcounts(R(j),Q(j)) + 1;
                end
                rqcounts = rqcounts(:);               
                logPrs_active = logPrs_active + ltr(:,:,k) * rqcounts;
            else
                % "Inactive" R
                NR2 = sumR(i,1,k) - NR;
                NR1 = NR-NR2;
                logPrs_inactive = logPrs_inactive + log_theta_r(k,1,1,1) ...
                    * NR1 + log_theta_r(k,2,1,1) * NR2;
            end
        end
        logPrs(:,i) = logPrs_active + logPrs_inactive;
    end
    logP = logPls' + logPrs';
    logP = logP - repmat(max(logP,[],2), 1, 2);
    
    return
end
    
