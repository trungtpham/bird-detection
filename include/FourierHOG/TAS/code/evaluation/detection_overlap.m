function ov = detection_overlap(bb, bbgt)
  bi=[max(bb(1),bbgt(1)) ; max(bb(2),bbgt(2)) ; min(bb(3),bbgt(3)) ; min(bb(4),bbgt(4))];
  bu=[min(bb(1),bbgt(1)) ; min(bb(2),bbgt(2)) ; max(bb(3),bbgt(3)) ; max(bb(4),bbgt(4))];
  iw=bi(3)-bi(1)+1;
  ih=bi(4)-bi(2)+1;
  if iw>0 & ih>0                
      ua=(bu(3)-bu(1)+1)*(bu(4)-bu(2)+1);
      ov=iw*ih/ua;
  else
      ov = 0;
  end  
  return
end
  
