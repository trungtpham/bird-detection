%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function pr = det_rpc(ins, pl, THRESH_OV, style)

ni=length(ins.name);
np=0;
ndets = 0;
dets = [];
recs = [];

% Allocate
det_counts = 0;
for i = 1:ni
    cands = ins.dets{i};
    det_counts = det_counts + size(cands,1);
end
dets(det_counts).imgnum = -1;
dets(det_counts).bbox   = [];
dets(det_counts).confidence = -inf;

% Copy
for i = 1:ni
    recs(i).objects = [];
    
    % DETECTIONS
    cands = ins.dets{i};
    for n = 1:size(cands,1)
        ndets = ndets + 1;
        dets(ndets).imgnum = i;
        dets(ndets).bbox = cands(n,:);
        dets(ndets).confidence = pl{i}(n);
    end    
    
    % GROUNDTRUTHS
    for n = 1:size(ins.gt{i},1)
        np = np + 1;
        recs(i).objects(n).det  = false;
        recs(i).objects(n).bbox = ins.gt{i}(n,:);
    end    
end

nd = length(dets);
rp=randperm(nd); % sort equal confidences randomly
dets=dets(rp);

if(~isempty(dets))
    [sc,si]=sort(-[dets(:).confidence]);
    dets=dets(:,si);
else
    si = [];
end

tp=zeros(nd,1);
for d=1:nd
    i=dets(d).imgnum;
    bb=dets(d).bbox;
    ovmax=-inf;
    for j=1:length(recs(i).objects)
        if(~isfield(recs(i).objects(j), 'bbox'))
            keyboard
        end 
        bbgt=recs(i).objects(j).bbox;
        ov = detection_overlap(bb,bbgt);
        if ov>ovmax
            ovmax=ov;
            jmax=j;
        end
        if ovmax >= THRESH_OV
            if ~recs(i).objects(jmax).det
                tp(d)=1;
                recs(i).objects(jmax).det=true;
            end
        end
    end
end

% REC & PREC
tp=cumsum(tp);
pr.recall = tp / (np+eps);
pr.precision=tp./(1:length(pr.recall))';
pr.f1 = (2*pr.recall.*pr.precision) ./ (pr.recall+pr.precision+eps);
[pr.max_f1 ind] = max(pr.f1);
pr.max_f1_thresh = dets(ind).confidence;

% "Average Precision"
pr.ap=0;
for t=0:0.1:1
    p = max(pr.precision(pr.recall>=t));
    if isempty(p), p=0; end
    pr.ap = pr.ap + p/11;
end

if(exist('style','var'))
    plot(pr.recall, pr.precision, style); 
    axis([0 1 0 1]);
    hold on;
    xlabel('Recall');
    ylabel('Precision');
end

return
end


