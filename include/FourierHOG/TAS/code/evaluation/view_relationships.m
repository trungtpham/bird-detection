function view_relationships(ins, index, r_index)

figure;
if(~exist('index','var')) 
    first_m = 1;
else
    first_m = index;
end

for m = first_m:length(ins.f)
    fprintf('%d\n', m);
    clf;
    J = size(ins.f{m},1);
%    Q = ins.q{m};
    order = randperm(100);
    for i = 1:size(ins.dets{m},1);
        clf;    
        I = imread(ins.image_filename{m});
        R = round(mean(I,3));
        G = R;
        B = R;        
        load(ins.seg_filename{m});
        rs = ins.r{m}(i,:,r_index);
        for j = 1:J
            if(rs(j)==2)
                R(S==j) = 255;
            end            
        end
        I = cat(3,R,G,B) / 256;
        imagesc(I);
        hold on;
        rectangle('Position', [ins.dets{m}(i,[1 2]) 1+abs(ins.dets{m}(i,[3 ...
                            4])-ins.dets{m}(i,[1 2]))], 'EdgeColor', ...
                  'r', 'LineWidth', 5);
        
        T = waitforbuttonpress;
        if(T == 0)
            return;
        end
        
    end
end
