function view_detections(ins, SCORE_BY, model, thresh)
% function view_detections(ins, SCORE_BY, model, thresh)
%
%  SCORE_BY: 0 = TRUE LABELS, 1 = B, 2 = Prior, 3 = Posterior
%

if(~exist('SCORE_BY', 'var')), SCORE_BY = 0; end;
if(~exist('thresh', 'var')), thresh = 0.1; end;

figure;
for m = 1:length(ins.image_filename)
    
    if(size(ins.dets{m}, 1) == 0), continue; end;
    if(size(ins.gt{m},1) == 0), continue; end;
    
    clf
    fprintf('Instance #%d\n', m);
    fprintf('Found %d candidates\n', size(ins.dets{m}, 1));
    I = im2double(imread(ins.image_filename{m}));
    imshow(I);
    hold on; axis off;
    for c = 1:size(ins.dets{m},1)        
        
        if(SCORE_BY==0)
            score = ins.truth_labels{m}(c) - 1;
        elseif(SCORE_BY==1) 
            score = min(1,exp(10*(ins.scores{m}(c)-1)));        
        elseif(SCORE_BY==2)
            score = exp(label_ll(2, ins.b{m}(c,:), model));
        elseif(SCORE_BY==3)
            score = ins.pl{m}(c); 
        end
        center = 0.5 * (ins.dets{m}(c,3:4)+ins.dets{m}(c,1:2));
        position = [ins.dets{m}(c,1:2) ins.dets{m}(c,3:4)- ...
                    ins.dets{m}(c,1:2)];
        w = 1; style = ':';
        if(score > thresh)
            w = 2; style = '-';
        end        
        rectangle('Position', position, 'EdgeColor', [score 0 1-score], ...
                  'LineWidth', w, 'LineStyle', style);                
    end
    t = waitforbuttonpress;    
    
end
close all;
return
end

function show_relationships(I, S, r, active_rs)
  ks = find(active_rs);
  for k = ks
      js = find(r(1,:,k)==2);
      for j = js
          Sj  = (S==j);
          Sj2 = imdilate(Sj,strel('disk',10));
          B = xor(Sj,Sj2);
          I(:,:,1) = max(I(:,:,1),B);
          I(:,:,2) = min(I(:,:,2),~B);
          I(:,:,3) = min(I(:,:,3),~B);
      end
  end  
  imagesc(I);
  return
end

function show_active_regions(SI, S, J, q)
  size(S)
  size(q)
  I2 = double(label2rgb(q(S))) / 256;
  SI = 0.5 * (SI + I2);
  imagesc(SI);
  return
end
