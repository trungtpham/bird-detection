% Assume that objects are sorted desendently according to their qualities.
function C =  eigen_bird_classification(data, threshold)

num = size(data,2);
C = zeros(1,num);
top = 200;
images = data(:, 1:top);
C(1:top) = 1;
test_images = data(:, top+1:end);
num_images = size(images,2);

% steps 1 and 2: find the mean image and the mean-shifted input images
mean_face = mean(images, 2);
shifted_images = images - repmat(mean_face, 1, num_images);
 
% steps 3 and 4: calculate the ordered eigenvectors and eigenvalues
[evectors, score, evalues] = princomp(images');
 
% step 5: only retain the top 'num_eigenfaces' eigenvectors (i.e. the principal components)
num_eigenfaces = 500;
evectors = evectors(:, 1:num_eigenfaces);
 
% step 6: project the images into the subspace to generate the feature vectors
features = evectors' * shifted_images;

% calculate the similarity of the input to each training image
for i=1:size(test_images,2)
    input_image = test_images(:,i);
    feature_vec = evectors' * (input_image - mean_face);
    similarity_score = arrayfun(@(n) 1 / (1 + norm(features(:,n) - feature_vec)), 1:num_images);
    % find the image with the highest similarity
    [match_score, match_ix] = max(similarity_score);
    if match_score > threshold
        C(i+top) = 1;
    end
    
end
 
end