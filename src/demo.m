% Train and test on individual image
clear all
close all
dbstop if error
addpath(genpath('../include'));
addpath(genpath('helpers'));

%% Detector and data parameters
param.feat = 'FHOG';
param.NMS_OV  = 0.3;
param.top = 20;
param.th = 0.0;
param.div = 3;
param.neighbour_scale = 10;
param.test_path = '../data/testing_images/';
param.training_pos_path = '../data/training_birds/';
param.training_neg_path = '../data/training_background/';


%% Which scene do you want to test? There are total 10 colonies, each has been captured at 4 different heights.
param.colony = '01'; % 01 02 03 ... 10
param.height = '60'; % 30, 60, 90, 120
param.train_per = 30; % training percentage from 1 to 30

% Reading testing image
I = imread(strcat(param.test_path, param.height, '/', param.colony, '.jpg'));
figure; imshow(I);
title('Input image');

%% Training bird detector
[model, param] = train_bird_detector(param);

%% Test the trained detector on the testing image
detections = bird_detector_global(I, model, param);
total_num = length(detections);
fprintf('Number of birds detected: %d \n', total_num);

%% Display result
close;
figure(2);imshow(I);
hold on;
for i=1:length(detections)
    cc = round(detections(i,1:2));
    plot(cc(1), cc(2), '+r');
end
str=sprintf('Number of detected birds: %d', length(detections));
title(str);





