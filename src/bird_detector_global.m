function detections = bird_detector_global(rgb_im, model, param)

birth_map = create_bird_map(rgb_im, param.bbRadius);
param.birth_map = birth_map;
I = rgb_im;
prepareKernel(param.featureScale, 4, 0);
[imArray, anchors] = im2blocks(I, param.bbRadius*3, param.div);
Y = cell(length(imArray),1);
for i=1:length(imArray)
    im = imArray{i};
    F = dense_feature_extractor(im, param.featureScale, param.feat);
    votes = F * model.w(1:end-1)' + model.w(end);
    Y_hat = reshape(votes, [size(im,1), size(im,2)]);
    Y{i} = Y_hat;
end

Y = blocks2im(Y, anchors, size(I,1), size(I,2));
bw = (Y > param.th);
bw = bw.*birth_map;

[ly, lx] = find(bw ==1);
cc = [lx, ly];
v = Y(bw==1);

bbRadius = param.bbRadius;
dt = [cc(:,1) - bbRadius , cc(:,2) - bbRadius , cc(:,1) + bbRadius , cc(:,2) + bbRadius , v];

dt = clipboxes(I, dt);
pick = nms(dt,param.NMS_OV);
dt = dt(pick,:);
cc = cc(pick,:);
v = v(pick);

[v, idx] = sort(v, 'descend');
cc = cc(idx,:);
dt = dt(idx,:);

g = rgb2hsv(rgb_im);
g = g(:,:,3);
g = imadjust(g);
param.I = g;
W =  bbRadius*2+1;
birds = cell(length(dt),1);
for i=1:length(dt)
    box = round(dt(i,1:4));
    duck = g(box(2):box(4), box(1):box(3),:);
    duck = imresize(duck, [W W]);
    birds{i} = duck;
end

C = v';
C = (C - min(C)) / (max(C) - min(C));
outlier_cost = 0.8;
N = length(v);
dc = [1 - C; outlier_cost*ones(1,N)];

edge_list = construct_neighbor_graph(cc, param.bbRadius*param.neighbour_scale*0.5);
edge_energy  = compute_pairwise_energy(edge_list, birds);

S = sparse(edge_list(:,1), edge_list(:,2), edge_energy, N, N);
S = S + S';

smoothness = 0.5;
pwc = smoothness*S;
h = BK_Create();
BK_AddVars(h,N);
BK_SetUnary(h,dc);
BK_SetNeighbors(h, pwc);
e = BK_Minimize(h);
labelling = BK_GetLabeling(h);
idx = labelling == 1;
cc = cc(idx,:);

N = length(cc);
DT = delaunayTriangulation(cc);
edge_list = edges(DT);
S = sparse(edge_list(:,1), edge_list(:,2), 1, N, N);
S = S + S';

D = pdist(cc);
D = squareform(D);
index = D > param.bbRadius*param.neighbour_scale;
S(index) = 0;
[L,C] = graphconncomp(S);
largest_comp = 1;
max_size = 1;
for i=1:L
    if sum(C==i) > max_size
        max_size = sum(C==i);
        largest_comp = i; 
    end
end
detections = cc(C==largest_comp,:);

end