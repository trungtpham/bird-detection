function [model, param] = train_bird_detector(param)
% parameters
train_per = param.train_per*3;
initrand();

% requires liblinear
addpath(genpath('liblinear-multicore-2.11-1'));

% requires the TAS package (and image data) from http://ai.stanford.edu/~gaheitz/Research/TAS/
% download and unfold it into the current path
% then add the path to the TAS package
addpath(genpath('TAS'));

%% load data and TAS setting
run('search_tas.m'); 
[Ipos,Ineg, mean_radius] = load_birds_dataset(param);

%%
Images = cell(length(Ipos),1);
GT_bbs = cell(length(Ipos),1);
for m = 1:length(Ipos)
    Images{m} = Ipos{m}.I;
    bbox = Ipos{m}.recs.objects.bbox;
    GT{m} = round(0.5 * (bbox(:,3:4) + bbox(:,1:2)));    %center
    GT_bbs{m} = bbox;
end
param.bbRadius = mean_radius;
param.indifferenceRadius  = param.bbRadius;
param.featureScale = ceil(param.bbRadius/3);

%% Computing features
reverseStr = '';
fprintf('Extracting FHOG features from positive images... ');
tic
PN = numel(Ipos);
PF = cell(1, PN);
prepareKernel(param.featureScale, 4, 0);
num_pos = length(Ipos);
for i = 1:num_pos
    I = Ipos{i}.I;
    F = dense_feature_extractor(I, param.featureScale, param.feat);
    PF{i} = F;
        
    % Display the progress
    percentDone = uint8(100 * i / num_pos);
    msg = sprintf('%d%%', percentDone); %Don't forget this semicolon
    fprintf([reverseStr, msg]);
    reverseStr = repmat(sprintf('\b'), 1, length(msg));
end
fprintf('\nFeature Dimension: %d \n' ,size(PF{i},2) );

% get mask and data
Pos = [];
for i = 1:length(Ipos)
    mask = zeros(size(Ipos{i}.I,1), size(Ipos{i}.I,2));
    mask(GT{i}(2), GT{i}(1)) = 1;
    se = ones(3);
    mask = imdilate(mask, se);
    Pos{i} = PF{i}(mask(:) == 1, :);
end
Pos = double(cell2mat(Pos'));
toc

p = randperm(size(Pos,1));
Pos = Pos(p,:);
Pos = Pos(1:round(length(p)*train_per*0.01),:);

NN = numel(Ineg);
NF = cell(1, NN);
Neg = [];
reverseStr = '';
fprintf('Extracting features from backgrounds... ');
tic
for i = 1:NN
    I = Ineg{i};
    F = dense_feature_extractor(I,param.featureScale, param.feat);
    NF{i} = F;
    N  = numel(I(:,:,1));
    idx = randsample(N, round(0.5*N));
    Neg{i} = F(idx, :);
    
    % Display the progress
    percentDone = uint8(100 * i / NN);
    msg = sprintf('%d%%', percentDone); %Don't forget this semicolon
    fprintf([reverseStr, msg]);
    reverseStr = repmat(sprintf('\b'), 1, length(msg));
end
fprintf('\n');
toc
Neg = double(cell2mat(Neg'));
pp = randperm(size(Neg,1));
Neg = Neg(pp,:);
Neg = Neg(1:size(Pos,1),:);


%%
fprintf('\nTrain linear SVM...\n');
tic
% simple normalization
ABSMAX = max([Pos;Neg]) + eps;
trainX = sparse(bsxfun(@times, [Pos;Neg], 1./ ABSMAX ));
model = train([ones(size(Pos,1),1); ones(size(Neg,1),1) * 0], trainX, '-B 1 -c 1');
model.w(1:end-1) = model.w(1:end-1) ./ ABSMAX;
toc
%% hard sample mining and retrain
fprintf('Hard sample mining...\n');
tic
HardNeg = cell(1,length(Ineg));
for i = 1:length(Ineg)
    
    %% detection
    I = double(Ineg{i});
    F = NF{i};
    votes =  F * model.w(1:end-1)' + model.w(end);
    Y_hat = reshape(votes, [size(I,1), size(I,2)]);
    
    %% sample negative samples based on the distances to the hyperplane
    Y_hat = max(Y_hat + 1, 0);
    index = [];
    N = numel(Y_hat);
    if max(Y_hat(:)) > 0
        index = unique(randsample(N, round(N*1), true, Y_hat(:)));
    end
    HardNeg{i} = F(index, :);
end
HardNegM = double(cell2mat(HardNeg'));

p = randperm(size(HardNegM,1));
HardNegM = HardNegM(p,:);
if length(HardNegM) > size(Pos,1)
    HardNegM = HardNegM(1:size(Pos,1),:);
end

%% train and predict
if (isempty(HardNegM) == 0)
    trainX = sparse(bsxfun(@times, [Pos;HardNegM], 1./ ABSMAX ));
    model = train([ones(size(Pos,1),1); zeros(size(HardNegM,1),1) ], trainX, '-B 1 -c 1');
    model.w(1:end-1) = model.w(1:end-1) ./ ABSMAX;
end
toc
end




