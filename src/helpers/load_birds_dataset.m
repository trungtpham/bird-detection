function [Ipos,Ineg,mean_radius] = load_birds_dataset(param)

birds_train = dir(strcat(param.training_pos_path, param.height, '/', param.colony));
birds_train(1:2) = [];
Npos = length(birds_train);

rotations = [0];

Ipos = cell(length(rotations)*Npos,1);
count = 1;
for i = 1:Npos
  rgb_im = imread(strcat(param.training_pos_path, param.height, '/', param.colony, '/', birds_train(i).name));
  [H, W, ~] = size(rgb_im);
     
  cx = round(W/2);
  cy = round(H/2);
  R(i) = 0.5*((W - 20*2)/2) + ((H - 20*2)/2);
      
  for j = 1:length(rotations)
      a = rotations(j);
      I = imrotate(rgb_im, a, 'bicubic');
      I = I(cy-20:cy+20, cx-20:cx+20,:);
      
      Ipos{count}.I = I;

      recs.folder = '';
      recs.filename = '';
      recs.source = '';
      [recs.size.width,recs.size.height,recs.size.depth] = size(I);
      recs.segmented = 0;
      recs.imgname = sprintf('%08d',i);
      recs.imgsize = size(I);
      recs.database = '';

      object.class = 'duck';
      object.view = '';
      object.truncated = 0;
      object.occluded = 0;
      object.difficult = 0;
      object.label = 'duck';
      object.bbox = [1 1 size(I,2) size(I,1)];
      object.bndbox.xmin =object.bbox(1);
      object.bndbox.ymin =object.bbox(2);
      object.bndbox.xmax =object.bbox(3);
      object.bndbox.ymax =object.bbox(4);
      object.polygon = [];
      recs.objects = [object];

      Ipos{count}.recs = recs;
      count = count + 1;
  end
end

mean_radius = ceil(mean(R))-2;

bg_dir = strcat(param.training_neg_path, param.height, '/', param.colony);
bg_train = dir(bg_dir);
bg_train(1:2) = [];
Nneg = length(bg_train);

Ineg = cell(Nneg,1);
for i = 1:Nneg
  I = imread(strcat(bg_dir, '/', bg_train(i).name));
  Ineg{i} = I;
end