function [mu, sig] = robust_normfit(data)
N = length(data);
mu = mean(data);
sig = sqrt(sum((data - mu).^2)/(N-1));
for i=1:3
    outliers = data > mu + 3*sig;
    data(outliers) = [];    
    mu = mean(data);
    sig = sqrt(sum((data - mu).^2)/(N-1));

end

end