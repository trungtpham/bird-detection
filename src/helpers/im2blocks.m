function [imArray, anchors] = im2blocks(I, BorderSize, num_block)

[rows, columns, ~] = size(I);

% Let's assume we know the block size and that all blocks will be the same size.
blockSizeR = round(rows/num_block); % Rows in block.
blockSizeC = round(columns/num_block); % Columns in block.

%blockSizeR = BorderSize*50; % Rows in block.
%blockSizeC = BorderSize*50; % Columns in block.


% Figure out the size of each block. 
wholeBlockRows = floor(rows / blockSizeR);
wholeBlockCols = floor(columns / blockSizeC);

imArray = cell(wholeBlockRows*wholeBlockCols,1);
anchors = cell(wholeBlockRows*wholeBlockCols,1);


% Now scan though, getting each block and putting it as a slice of a 3D array.
sliceNumber = 1;
row = 1;
while row < rows - BorderSize
    col = 1;
    while col < columns - BorderSize    
        anchors{sliceNumber} = [row col];
        
        row1 = anchors{sliceNumber}(1);
        row2 = min(row1 + blockSizeR - 1, rows);
        
        col1 = anchors{sliceNumber}(2);
        col2 = min(col1 + blockSizeC - 1, columns);
        
        % Extract out the block into a single subimage.
        oneBlock = I(row1:row2, col1:col2,:);
        %oneBlock = imadjust(oneBlock);
        % Assign this slice to the image we just extracted.
        imArray{sliceNumber} = oneBlock;
        sliceNumber = sliceNumber + 1;
        col = col2 - BorderSize;
    end
    row = row2 - BorderSize;
end

end