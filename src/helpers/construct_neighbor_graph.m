function edge_list = construct_neighbor_graph(data, radius)

N = size(data,1);
D = pdist(data);
D = squareform(D);
D(D < radius ) = 1;
D(1:N+1:N*N) = 0;
D = triu(D);
[x, y] = find(D == 1);
edge_list = [x y];

end