function F = dense_feature_extractor(I, scale, method)

if strcmp(method, 'FHOG')
    I = im2double(I);
    F = FourierHOG(I, scale);
end

if strcmp(method, 'LBP')
    I = rgb2gray(I);
    I = single(I);
    F = dense_vl_lbp(I, scale);
    F = reshape(F, [size(I,1)*size(I,2) 58]);
end


end