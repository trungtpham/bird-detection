function Y = blocks2im(imArray, anchors, H, W)

num_blocks = length(imArray);

I = -100*ones(H, W, num_blocks);
for i=1:num_blocks
    loc = anchors{i};
    lx = loc(2);
    ly = loc(1);
    J = imArray{i};
    [h, w] = size(J);
    I(ly:ly+h-1,lx:lx+w-1, i) = J;
    
end

Y = max(I, [], 3);

end