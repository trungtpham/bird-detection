for h=30:30:120
    for c=1:10
        file_name = strcat('Results/', num2str(h), '/', sprintf('%02d',c), '/', 'train%_count.mat');
        load(file_name);
        des_file_name = strcat('Results/', num2str(h), '/', sprintf('%02d',c), '/', 'train%_count.cvs');
        p = Results(:,1);
        p = round(p*30/100);
        count = Results(:,2);
        R = [p, count];
        csvwrite(des_file_name, R);
    end
end