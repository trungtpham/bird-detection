% Assume that objects are sorted desendently according to their qualities.
function [C, cc, v, birds] =  template_matching(data, cc, param)

% extract top data
centre = mean(cc);
D = pdist2(cc, centre);
[~, top_idx] = sort(D);
top_idx = top_idx(1:param.top);
% top_data = data(1:param.top);
% cc_top = cc(1:param.top,:);
% DT = delaunayTriangulation(cc_top);
% edge_list = edges(DT);
% S = sparse(edge_list(:,1), edge_list(:,2), 1, param.top, param.top);
% S = S + S';
% 
% D = pdist(cc_top);
% D = squareform(D);
% index = D > param.bbRadius*5;
% S(index) = 0;
% [L,C] = graphconncomp(S);
% largest_comp = 1;
% max_size = 1;
% for i=1:L
%     if sum(C==i) > max_size
%         max_size = sum(C==i);
%         largest_comp = i; 
%     end
% end
% 
% top_idx = find(C==largest_comp);

% Prepare templates
count = 1;
templates = cell(length(top_idx)*4,1);
%figure; imshow(param.I); hold on;
for i=1:length(top_idx);
    idx = top_idx(i);
    %plot(cc(idx,1), cc(idx,2), '+r');
    T = data{idx};
    T2 = flip(T,2);
    T3 = flip(T,1);
    T4 = flip(T3,2);
    templates{count} = T;
    templates{count+1} = T2;
    templates{count+2} = T3;
    templates{count+3} = T4;
    count = count + 4;
end

num_templates = length(templates);
I = param.I;
ps = param.bbRadius;
score_map = zeros(size(I));
for j=1:num_templates
    T = templates{j};
    c = normxcorr2(T,I);
    c = c(ps+1:end-ps, ps+1:end-ps);
    score_map = max(c, score_map);
end

linearInd = sub2ind(size(I), cc(:,2), cc(:,1));
C = score_map(linearInd);

%bw = imregionalmax(score_map);
bw = (score_map > 0.7);
bw = bw.*param.birth_map;

[ly, lx] = find(bw ==1);
cc = [lx, ly];
v = score_map(bw==1);
bbRadius = param.bbRadius;
dt = [cc(:,1) - bbRadius , cc(:,2) - bbRadius , cc(:,1) + bbRadius , cc(:,2) + bbRadius , v];
dt = clipboxes(I, dt);
pick = nms(dt,param.NMS_OV);
dt = dt(pick,:);
cc = cc(pick,:);
v = v(pick);
W =  param.bbRadius*2+1;
birds = cell(length(dt),1);
for i=1:length(dt)
    box = round(dt(i,1:4));
    duck = I(box(2):box(4), box(1):box(3),:);
    duck = imresize(duck, [W W]);
    birds{i} = duck;
end

end
