% Assume that objects are sorted desendently according to their qualities.
function C =  feature_matching(data, param)

num = size(data,3);
prepareKernel(param.featureScale, 4, 0);
c_idx = sub2ind([size(data,1), size(data,2)], round(size(data,1)/2), round(size(data,1)/2));
for i = 1:num
    I = double(data(:,:,i));    
    F = FourierHOG(I,param.featureScale);
    %FF(i,:) = F(c_idx,:);
    FF(i,:) = F(:);
end
top = param.top;

templates_feat = FF(1:top,:);

D = pdist2(FF, templates_feat, 'cosine');
C = min(D,[],2);
C = C/max(C(:));

end