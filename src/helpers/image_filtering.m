% Pre processing image
clear all
close all
I = imread('../Bird Counting/Test Images/120.jpg');
%I = igamma(I, 'sRGB');
imshow(I);
hsv_im = rgb2hsv(I); 
s_im = hsv_im(:,:,3);
se = ones(3);
f_im = imdilate(1-s_im,se);
f_im = imadjust(f_im);
bw = f_im > 0.5;
imshow(f_im);
figure; imshow(bw)
CC = bwconncomp(bw);

for i=1:CC.NumObjects
    S = CC.PixelIdxList{i};
    
    if length(S) > 100
        bw(S) = 0;
    end
end