function E  = compute_pairwise_energy(edge_list, data)

num_edges = size(edge_list,1);
E = zeros(num_edges,1);
for i=1:num_edges
    from = edge_list(i,1);
    to = edge_list(i,2);
    A = data{from};
    B = data{to};
    sc = normxcorr2(A,B);
    E(i) = max(sc(:));
end
E = max(0, E);
E(E < 0.4) = 0;
E = (E - min(E)) / ( max(E) - min(E));
end

% function ncc = normcxcorr(A,B)
% N = length(A);
% ma = mean(A);
% mb = mean(B);
% siga = std(A);
% sigb = std(B);
% ncc = sum((A-ma).*(B-mb)./(siga*sigb))/N;
% end