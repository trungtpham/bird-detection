function bw = create_bird_map(I, object_size)
max_area = 3*((object_size*2)^2);
im = rgb2gray(I);
im = im2double(im);
im = imgaussfilt(im,2);
im = histeq(im);
BW = imregionalmax(1-im);
bw = (im < 0.1).*BW;
CC = bwconncomp(bw);
for i=1:CC.NumObjects
    S = CC.PixelIdxList{i};
    if length(S) > max_area
        bw(S) = 0;
    end
end
bw = imdilate(bw, strel('disk', 10));
end