#Semi-automated bird detection from drone-derived imagery

## Introduction

This is an open source Matlab implementation for 
J. C. Hodgson et al "Drones count wildlife more accurately and precisely than humans" (currently under review). 

[![Creative Commons License](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

The source code and dataset are published under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/)


Current version is 1.0.


## Structure
* include (external libraries)
	* [FourierHOG](http://lmb.informatik.uni-freiburg.de/resources/opensource/FourierHOG/) 
	* [maxflow](http://vision.csd.uwo.ca/code/)
* data
	* contains both training and testing data.
* src
	* contains the bird detection algorithm.

## Compilation
Open Matlab
```
cd include/Bk_matlab
BK_UnitTest.m
cd ../..
cd include/FourierHOG/liblinear-multicore-2.11-1/matlab
make.m
```
## Run demo
```
demo.m
```
The code has been tested under Ubuntu 16.04 and Matlab 2016a.
## Contact
* Trung Pham (trung.pham@adelaide.edu.au)
* Jarrod Hodgson (jarrod.hodgson@adelaide.edu.au)
